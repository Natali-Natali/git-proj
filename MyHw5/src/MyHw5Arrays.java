import java.util.Scanner;
import java.util.Arrays;

public class MyHw5Arrays {
    public static void main(String[] args) {
        int i=0, n, lm, s=0; // i-ind of arr * lm-длинна массива * s - summa
        System.out.println("Задача-заполняем массив и считаем его сумму");
        System.out.print("Введите число от 1 до 100: ");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        lm  = n/2;
        //System.out.println("lm " + lm);
        int[] myArr;
        myArr = new int[lm];
        System.out.println("Элементы массива: ");

        for ( i=0; i<=lm-1; i++ ) {
            myArr[i] = (i+1)*2;
            // содержимое ячеек вычисляется через порядковый номер массива. Элементы - четные числа начиная с 2
            System.out.print("  " + myArr[i]);
            s = s+myArr[i];
        }
        System.out.println(" ");
        System.out.println("Сумма элементов массива = " + s);
    }

}
 /*   for ( i=0; i<=lm-1; i++ ) {
            myArr[i] = j;
            System.out.print("  " + myArr[i]);
            s = s+myArr[i];
            j=j+2;
        } */